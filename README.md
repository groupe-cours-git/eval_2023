Évaluation Git 2023
===================

_**NB:** Cette évaluation des cours sur Linux et Git a été donnée en
janvier 2023. L'évaluation finale de cette année sera semblable.
Durée prévue: 1h._

L'évaluation porte sur une partie pratique et une partie théorique. Elle
est individuelle (pas de communication entre vous), et vous avez le
droit aux cours et documentations.

Préparation
-----------

1.  Faire un *fork* de ce dépôt dans votre compte Gitlab, en gardant un
    **niveau de visibilité privé**.
2.  Depuis Gitlab, sur la page de votre dépôt, dans la barre de gauche,
    aller sur « **Project information** », cliquer sur « **Members** »,
    puis sur la page qui s'affiche, « **Invite members** ». Dans la
    fenêtre qui s'ouvre, taper `jeanjil`, cliquer sur mon compte,
    sélectionner le rôle de « **Developer** », et valider en cliquant
    sur « **Invite** ».

*Si cette partie pose problème, lever la main et attendre que j'arrive.*

3.  Cloner ce dépôt sur votre ordinateur.

Théorie
-------

Sur votre ordinateur, éditer ce fichier pour répondre (en sautant des
lignes entre les questions) aux questions suivantes :

4.  À quoi sert un système de gestion de version ?


5.  Pourquoi le logiciel Git a-t-il été créé ? (quel contexte?)


6.  Quelle ligne de commande permet de voir *tous* les fichiers du
    répertoire courant ?


7.  Je suis dans le dossier `~/tp/exo1/` et j'entre la commande
    `cd ../../cm/linux`. Où suis-je ?


8.  Que fait la commande `mkdir tmp` ?


9.  Comment peut-t-on créer un nouveau fichier `README.md` à partir de
    la console ?


10. Quel est l'intérêt du fichier `README.md` dans un dépôt Git ?


11. Quelle commande permet de visualiser l'historique du dépôt ?


12. Je dispose de l'historique suivant :
    ```
    0f64252 (HEAD -> master) Corrige énorme bug !
    6c0cae8 Ajoute Prison et Policier
    52ce28a Ajoute Propriétés
    64e11c7 (tag: v1.0) Ajoute main.cpp
    4277f95 Premier commit, README.md
    ```
    Je veux ramener mon répertoire de travail dans l'état du second
    commit (« Ajoute main.cpp »). Il y a au moins trois manières de
    faire ; en donner deux.


13. Mettre ces trois actions dans un ordre logique : Indexation,
    Modification, Validation.


14. La commande `git status` me donne le résultat suivant :
    ```
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git restore <file>..." to discard changes in working directory)
          modified:   Cours_Proba.tex
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)
          Cours_Stats.tex
    ```
    Que sera le résultat de la commande `git commit -am "Un message"` ?

Pratique
--------

15. Valider les modifications de ce fichier en effectuant un commit.
16. Pousser votre (ou vos) commit(s) sur votre dépôt Gitlab, vérifier
    que vos réponses apparaissent convenablement.
17. Avec les commandes Git vues en cours (et / ou en cherchant dans la
    doc si besoin), manipuler votre dépôt pour que le résultat de la
    commande `git log --oneline --graph` ressemble à ça :
    ```
    *   f2faa20 (HEAD -> master, tag: v1) Merge branch 'dev'
    |\
    | * f21556c (dev) Ajoute test.txt et modifie eval.txt
    | * 1866d96 Modifie eval.txt
    * | de951f5 Modifie eval.txt
    |/
    * 80f4e6e (tag: v0) Ajoute eval.txt
    ```
    (seuls les derniers commits seront pris en compte)
18. Pousser toutes les branches et tags sur votre dépôt distant en
    tapant : `git push --all && git push --tags`.

Avant de partir, vérifier qu'il n'y a pas d'erreur et que la liste de
tous vos commits apparaît sur votre dépôt Gitlab (Voir dans « Repository
→ Commits » ou mieux, « Repository → Graph »). **Attention** : ce qui
n'est pas dans votre dépôt distant ne sera pas pris en compte (parce que
je ne le verrai pas).
